# Développement Web : déploiement Heroku

> Initiation au déploiement d'une application web sur [Heroku][heroku]

Lors cette activité nous allons déployer une application web Flask
sur la plateforme d'hébergement [Heroku][heroku]. À titre d'exemple,
vous trouverez une application d'exemple dans ce dossier pour vérifier
votre mise en place. Dans un second temps vous pourrez reproduire et
adapter les étapes à l'application de votre choix.

## Recherche d'information

- [ ] Quels sont les grands types d'hébergeurs d'applications web ?
- [ ] À quel type d'hébergement appartient la plateforme Heroku ?
- [ ] Quels autres hébergeurs proposent un service équivalent à Heroku ?
- [ ] Quels environnements sont supportés par la plateforme Heroku ?
- [ ] Quelle pratique moderne permet d'automatiser le déploiement d'applications ?

## Déploiement Manuel

Dans un premier temps, nous allons déployer une application web
manuellement depuis notre machine personnelle.

Voici les étapes à suivre :

1. Créer un compte gratuit Heroku

2. Dans votre dashboard Heroku : créer une nouvelle application vide dans votre espace et prenez note de son nom unique

3. Dans vos paramètres personnels Heroku, section "Applications" : créer un token d'autorisation

4. Créer un nouveau dépôt Git:
```bash
git init
```

5. Ajouter et commiter les fichiers de l'application Flask d'exemple:
```bash
git add app.py Procfile requirements.txt
git commit -m "Add example Flask app"
```

6. Ajouter le remote Git Heroku de votre application en remplaçant `HEROKU_APP` par le nom de votre application et `HEROKU_TOKEN` par le token d'autorisation créé :
```bash
git remote add heroku https://heroku:HEROKU_TOKEN@git.heroku.com/HEROKU_APP.git
```

7. Pousser le dépôt Git chez Heroku pour lancer le déploiement :
```bash
git push heroku main
```

8. Si le déploiement s'est déroulé sans problème, vous pouvez accéder à
   votre application via son URL du type `https://HEROKU_APP.herokuapp.com`

Bravo, vous venez d'effectuer votre premier déploiement d'application web
dans le cloud !

Vous pouvez modifier le fichier `app.py`, commiter les changements et
les pousser sur Heroku pour mettre à jour votre déploiement !

Si vous souhaitez aller plus loin, n'hésitez pas à consulter la
documentation Heroku en ligne, particulièrement bien fournie en
exemples et guides.

## Déploiement Automatisé

Passons désormais à l'automatisation de notre déploiement.
Nous souhaitons déployer l'application à chaque fois qu'un.e développeur.se
pousse sur la branche `main` de notre dépôt GitLab. Cela permet de
s'assurer que l'application en ligne est toujours à jour au fur et à
mesure des nouveaux développements. C'est ce que l'on appelle le
_déploiement continu_.

Par chance, GitLab intègre un service d'intégration et déploiement continu
appelé GitLab-CI. Nous allons utiliser ce service pour automatiser notre
déploiement.

Voici les étapes :

1. Créer un dépôt GitLab d'exemple

2. Ajouter le remote Git de ce dépôt avec une commande du type :
```bash
git remote add origin git@gitlab.com:USERNAME/REPOSITORY.git
```

3. Ajouter un fichier nommé `.gitlab-ci.yml` avec le contenu suivant :
```yaml
image: python:3.9

stages:
  - deploy

deploy:
  stage: deploy
  script:
    - git remote add heroku https://heroku:$HEROKU_TOKEN@git.heroku.com/$HEROKU_APP.git
    - git push heroku HEAD:refs/heads/master
```

4. Dans le dépôt GitLab, section "Settings - CI/CD - Variables", ajouter les variables d'environnement suivantes :
    - `HEROKU_APP` : nom unique de votre application
    - `HEROKU_TOKEN` : le token d'autorisation créé précédemment

5. Pousser le dépôt Git sur GitLab pour lancer le déploiement :
```bash
git push origin main
```

6. Observer le bon déroulement du déploiement dans la section "CI/CD" du
   dépôt GitLab

7. Si le déploiement s'est déroulé sans problème, vous pouvez accéder à
   votre application via son URL du type `https://HEROKU_APP.herokuapp.com`

Vous pouvez maintenant modifier le fichier `app.py`, commiter sur
`main `et pousser sur GitLab pour mettre à jour automatiquement
l'application en ligne !

## Pour aller plus loin

Cette activité n'est qu'une initiation / découverte du déploiement
d'application web avec Heroku. Pour aller plus loin, vous pouvez
explorer les sujets suivants (liste non exhaustive) :

- [ ] Comment définir et utiliser des variables d'environnement sur Heroku ?
- [ ] Comment configurer le fichier `Procfile` ?
- [ ] Comment exécuter à distance une commande admin définie par notre application (par exemple avec Flask CLI) ?
- [ ] Comment remplacer la gestion des dépendances Python avec `pipenv` ?
- [ ] Comment exécuter plusieurs processus en parallèle ?


[heroku]: https://www.heroku.com "Heroku"
[heroku-cli]: https://devcenter.heroku.com/articles/heroku-cli "Heroku CLI"
