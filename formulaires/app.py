from base64 import b64encode
from flask import Flask, jsonify, render_template, request


app = Flask(__name__)


@app.route("/", methods=["GET", "POST"])
def home():
    if request.method == "POST":
        first_name = request.form.get("first_name")
        last_name = request.form.get("last_name")
        avatar_file = request.files["avatar"]
        avatar = {
            "mimetype": avatar_file.mimetype,
            "base64": b64encode(avatar_file.stream.read()).decode("utf-8"),
        }
    else:
        first_name = None
        last_name = None
        avatar = None

    return render_template(
        "home.html",
        fname=first_name,
        lname=last_name,
        avatar=avatar,
    )


@app.post("/api")
def process_image():
    image_file = request.files["avatar"]
    return jsonify({
        "filename": image_file.filename,
        "mimetype": image_file.mimetype,
        "content_length": len(image_file.stream.read()),
    })
